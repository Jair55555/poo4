function llenar(){
    var limite = document.getElementById('limite').value;
    var Listanumeros = document.getElementById('numeros');
    let par = document.getElementById('porPar');
    let imp = document.getElementById('porImpar');
    let simetria = document.getElementById('Simetrico');
    var arreglo = [];

    // Limpiar las opciones del select
    while(Listanumeros.options.length>0){
        Listanumeros.remove(0);
    }

    for(let con = 0; con < limite; con++) {
        let aleatorio = Math.floor(Math.random()*(50)+1); 
        Listanumeros.options[con] = new Option(aleatorio, 'valor:' + con);
        arreglo[con] = aleatorio;
    
    }
    
    let orden = ordenarValoresSelect(arreglo);

    for(let con = 0; con<limite; con++){
        Listanumeros.options[con] = new Option(orden[con]);
    }

    par.innerHTML = valorPares(arreglo).toFixed(2) + "%"; 
    imp.innerHTML = valorImpares(arreglo).toFixed(2) + "%"; 

    
    let pares = valorPares(arreglo);
    let impares = valorImpares(arreglo);

    if(pares - impares > 25 || impares - pares > 25){
        simetria.innerHTML = "No es Simétrico";
    }else {
        simetria.innerHTML = "Si es Simétrico";
    }



    // hacer commit 'Generacion de numeros aleatorios -listo
    // hacer commit 'Validacion de caja de texto (limite) REQUERIDO Y NUMERICO
    // hacer commit con la listanumeros ORDENADOS ASCENDENTE
}

function alerta(){
    valor = document.getElementById('limite').value;
    errorValor = document.getElementById('errorLimite');
    porP = document.getElementById('porPar');
    porImp = document.getElementById('porImpar');
    sim = document.getElementById('Simetrico');

    valor== "" || valor== 0 ? errorValor.style.visibility = 'visible': errorValor.style.visibility = 'hidden' ;

    if(valor==0){

        alert("Faltan datos por capturar");
        porP.innerHTML = "";
        porImp.innerHTML = "";
        sim.innerHTML = "";
    }
}

// ordenar menor a mayor
function ordenarValoresSelect(numeros){
    let arr = numeros, longitudOrdenMayor = numeros.length;
    let band = false;

    while(!band){
        band = true;
        for(let i=0; i<longitudOrdenMayor; i++){
                    // [2] 97                          [1] 12
            if(arr[i] > arr[i+1]){
                aux = arr[i+1];   //aux = 12
                arr [i+1] = arr [i]; // numerosDesordenados[1] = 97
                arr[i] = aux; // numerosDesordenados [2] = 12
                band = false;
            }
        }
    }
    return arr;
}

// contar los numeros pares e impares para saber su porcentaje
// la diferencia no sea mayor al 25%

// contar numeros pares e impartes
// contar numeros pares
function valorPares(numeros){
    let contador = 0; 
    let arr = numeros;
    let numerosL = numeros.length;
    for(let i=0; i<numerosL; i++){
        if(arr[i]%2 == 0){ 

            contador++;
        }
    }
    contador = ((contador * 100) / numerosL);
    return contador;

    

}

// contar numeros impares
function valorImpares(numeros){
    let contador = 0; 
    let arr = numeros;
    let numerosL = numeros.length;
    for(let i=0; i<numerosL; i++){
        if(arr[i]%2 != 0){ 

            contador++;
        }
    }
    contador = ((contador * 100) / numerosL);
    return contador;

}